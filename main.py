import json

from flask import Flask, jsonify, request

from gitlab_bot.constants import PARSE_MAPPING
from gitlab_bot.settings import bot
from gitlab_bot.utils import get_redis_instance

app = Flask(__name__)


@app.route('/<uuid:uuid>/', methods=['GET', 'POST'])
def gitlab_view(uuid) -> json:
    if request.method == 'POST':
        object_kind = request.json['object_kind']
        klass = PARSE_MAPPING[object_kind]
        instance = klass(request.json)
        msg = instance.make_message()
        redis_instance = get_redis_instance()
        chat_id = redis_instance.get(str(uuid))
        bot.send_message(chat_id=chat_id, text=msg, parse_mode='MARKDOWN')

    return jsonify(status='success')


if __name__ == '__main__':
	app.run(debug=True, host='0.0.0.0', port=5000)
