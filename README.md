# Telegram GitLab Bot

Receive notifications from your GitLab to Telegram

You can add this bot follow this link: https://t.me/TheGitLabBot

# Requirements

* Docker
* Composer

# Installation

Build project using Docker

~~~
docker-compose build
~~~

After successfully built start project using follow command

~~~
docker-compose up
~~~

You must create ```.env``` file. It must be on your project root directory. 
You can see example in ```.env.example``` file with necessary settings on your project root directory.

# Using

You can access shell via

~~~
docker exec -ti telegram-gitlab-bot_app_1 sh
~~~

# Code syntax

We using PEP8 as code syntax rules. Max length of line is 100 symbols.
You can use `pylint` for checking style. For example:

~~~
docker-compose run --no-deps -T app find ./ -name "*.py" -exec pylint --exit-zero "{}" +
~~~

Additionally you can check and fix imports via

~~~
docker-compose run --no-deps -T app isort -rc
~~~
