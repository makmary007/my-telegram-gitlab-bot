from urllib.parse import urljoin

from gitlab_bot.settings import PROJECT_URL, bot
from gitlab_bot.utils import gen_url_suffix, get_redis_instance


@bot.message_handler(commands=['start'])
def starting_message(message):
	"""
	We save chat_id to redis as {'url_suffix':'chat_id'}
	"""
	url_suffix = gen_url_suffix()
	user_url = urljoin(PROJECT_URL, url_suffix)
	redis_instance = get_redis_instance()
	chat_id = message.chat.id
	redis_instance.set(url_suffix, chat_id)
	bot.send_message(message.chat.id, user_url)

@bot.message_handler(commands=['help'])
def help_message(message):
	bot.send_message(message.chat.id, "This is help")

@bot.message_handler()
def all_message(message):
	bot.send_message(message.chat.id, "Test!!")


if __name__ == '__main__':
	bot.polling()
