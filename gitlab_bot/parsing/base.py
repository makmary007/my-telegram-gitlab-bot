from abc import ABC, abstractmethod


class BaseParser(ABC):

    def __init__(self, data: dict):
        self.data = data

    def _get_assignees(self) -> str or None:
        if self.data.get('assignees'):
            assignee_list = [key.get('username') for key in self.data.get('assignees')]
            assignes_username = ', '.join(assignee_list)
            return assignes_username

    @abstractmethod
    def parse(self) -> dict:
        pass

    @abstractmethod
    def make_message(self) -> str:
        pass
