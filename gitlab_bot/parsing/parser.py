from gitlab_bot.parsing.base import BaseParser
from gitlab_bot.parsing.comment import (CommentCommitParser,
                                        CommentIssueParser,
                                        CommentMergeRequestParser)


class IssueParser(BaseParser):

    def parse(self) -> dict:
        username = self.data['user']['name']
        project_url = self.data['project']['web_url']
        project_name = self.data['project']['name']
        title = self.data['object_attributes']['title']
        issue_url = self.data['object_attributes']['url']

        assignes_username = self._get_assignees()

        parse_dict = {
            'username': username,
            'project_url': project_url,
            'project_name': project_name,
            'title': title,
            'issue_url':  issue_url,
            'assignes_username': assignes_username
        }

        return parse_dict

    def make_message(self) -> str:
        """
        Example message:

        Created issue [Add tests](https://gitlab.com/test/test_project/-/issues/1) by User1
        in [Test Project](https://gitlab.com/test/test_project), assigned to User2.
        """
        parse_dict = self.parse()
        username = parse_dict['username']
        project_url = parse_dict['project_url']
        project_name = parse_dict['project_name']
        title = parse_dict['title']
        issue_url = parse_dict['issue_url']
        md_issue_link = f'[{title}]({issue_url})'
        md_project_link = f'[{project_name}]({project_url})'

        base_str = f'Created issue {md_issue_link} by {username} in {md_project_link}'

        if parse_dict.get('assignes_username'):
            assignes_username = parse_dict.get('assignes_username')
            assigne_str = f'assigned to {assignes_username}'
            return f'{base_str} {assigne_str}'

        return base_str


class PushParser(BaseParser):

    def parse(self) -> dict:
        username = self.data['user_name']
        project_name = self.data['project']['name']
        project_url = self.data['project']['web_url']

        parse_dict = {
            'username': username,
            'project_url': project_url,
            'project_name': project_name,
        }

        if self.data.get('commits'):
            parse_dict['list_commits_dict'] = self.data.get('commits')

        return parse_dict

    def _make_pushed_msg(self, username: str, md_project_link: str) -> str:
	    return f'{username} pushed to {md_project_link}'

    def _make_commit_msg(self, d: dict) -> str:
        if d:
            author = d['author']['name']
            title = d['title']
            url = d['url']
            return f'\* {author}: [{title}]({url})'

    def _make_commits(self) -> str:
        parse_dict = self.parse()
        if parse_dict.get('list_commits_dict'):
            list_commits_dict = parse_dict['list_commits_dict']
            commits = []
            for d in list_commits_dict:
                commits.append(self._make_commit_msg(d))
            return '\n'.join(commits)

    def make_message(self) -> str:
        """
        Example message:

        User1 pushed to [Test Project](https://gitlab.com/test/test_project).

        Commits are:

        * User2: [Merge branch 'test' into 'master'](https://gitlab.com/test/test_project/-/commit/dfa3f4baa77c48c1ba1d95ca31074ac1)
        * User1: [#4 Add tests](https://gitlab.com/test/test_project/-/commit/58af762d-dae14e79b85dfc3aceb0d69f)
        * User3: [#4 Add docstrings](https://gitlab.com/test/test_project/-/commit/b76ac74736864ded9a269ae05c84bf3e)
        """

        parse_dict = self.parse()
        username = parse_dict['username']
        project_name = parse_dict['project_name']
        project_url = parse_dict['project_url']
        md_project_link = f'[{project_name}]({project_url})'

        pushed_msg = self._make_pushed_msg(username, md_project_link)

        return(
            f'{pushed_msg}.\n\n'
            'Commits are:\n\n'
            f'{self._make_commits()}'
        )


class MergeParser(BaseParser):

    def parse(self) -> dict:
        username = self.data['user']['name']
        project_url = self.data['project']['web_url']
        project_name = self.data['project']['name']
        mr_url = self.data['object_attributes']['url']
        title = self.data['object_attributes']['title']
        work_in_progress = self.data['object_attributes']['work_in_progress']

        assignes_username = self._get_assignees()

        parse_dict = {
            'username': username,
            'project_url': project_url,
            'project_name': project_name,
            'title': title,
            'mr_url': mr_url,
            'work_in_progress': work_in_progress,
            'assignes_username': assignes_username
        }

        return parse_dict

    def make_message(self) -> str:
        """
        Example message:

        User1 opened [WIP: Update to django 3](https://gitlab.com/test/test_project/-/merge_requests/1)
        in [Test Project](https://gitlab.com/test/test_project), assigned to User2.
        """

        parse_dict = self.parse()
        username = parse_dict['username']
        project_url = parse_dict['project_url']
        project_name = parse_dict['project_name']
        title = parse_dict['title']
        mr_url = parse_dict['mr_url']
        assignes_username = parse_dict['assignes_username']
        md_mr_link = f'[{title}]({mr_url})'
        md_project_link = f'[{project_name}]({project_url})'

        base_str = f'{username} opened {md_mr_link} in {md_project_link}'

        if parse_dict.get('work_in_progress'):
            title = f'WIP: {title}'

        return f'{base_str}, assigned to {assignes_username}.'


class CommentParser(BaseParser):

    COMMENT_MAPPING = {
        'Issue': CommentIssueParser,
        'Commit': CommentCommitParser,
        'MergeRequest': CommentMergeRequestParser
    }

    def __init__(self, data: dict):
        event_type = data['object_attributes']['noteable_type']
        klass = self.COMMENT_MAPPING[event_type]
        self._child = klass(data)

    def parse(self) -> dict:
        return self._child.parse()

    def make_message(self) -> str:
        return self._child.make_message()
