from gitlab_bot.parsing.base import BaseParser


class MessageMixin:

    @staticmethod
    def _make_comment_msg(event_type: str, link: str, username: str, note: str,
                          md_project_link: str):
        return f'In {event_type} {link} {username} added comment {note} in {md_project_link}'

    def make_message(self) -> str:
        """
        Example message:

        In Issue/Commit/MergeRequest [Add tests](https://gitlab.com/test/test_project/-/issues/1)
        User1 added comment "test" in [Test Project](https://gitlab.com/test/test_project).
        """

        parse_dict = self.parse()
        event_type = parse_dict['event_type']
        username = parse_dict['username']
        project_url = parse_dict['project_url']
        note = parse_dict['note']
        note = f'"{note}"'
        project_name = parse_dict['project_name']
        md_project_link = f'[{project_name}]({project_url})'

        title = parse_dict['title']
        url = parse_dict['url']
        link = f'[{title}]({url})'

        base_str = self.__class__._make_comment_msg(event_type, link, username, note, md_project_link)

        return base_str


class CommentIssueParser(MessageMixin, BaseParser):

    def parse(self) -> dict:
        event_type = self.data['object_attributes']['noteable_type']

        username = self.data['user']['name']
        project_url = self.data['project']['web_url']
        project_name = self.data['project']['name']
        note = self.data['object_attributes']['note']

        issue_url = self.data.get('issue').get('url')
        issue_title = self.data.get('issue').get('title')

        parse_dict = {
            'username': username,
            'project_url': project_url,
            'project_name': project_name,
            'note': note,
            'event_type': event_type,
            'title': issue_title,
            'url': issue_url
        }

        return parse_dict


class CommentCommitParser(MessageMixin, BaseParser):

    def parse(self) -> dict:
        event_type = self.data['object_attributes']['noteable_type']

        username = self.data['user']['name']
        project_url = self.data['project']['web_url']
        project_name = self.data['project']['name']
        note = self.data['object_attributes']['note']

        commit_title = self.data.get('commit').get('message')
        commit_url = self.data.get('commit').get('url')

        parse_dict = {
            'username': username,
            'project_url': project_url,
            'project_name': project_name,
            'note': note,
            'event_type': event_type,
            'title': commit_title,
            'url': commit_url
        }

        return parse_dict


class CommentMergeRequestParser(MessageMixin, BaseParser):

    def parse(self) -> dict:
        event_type = self.data['object_attributes']['noteable_type']

        username = self.data['user']['name']
        project_url = self.data['project']['web_url']
        project_name = self.data['project']['name']
        note = self.data['object_attributes']['note']

        merge_title = self.data.get('merge_request').get('title')
        merge_url = self.data.get('object_attributes').get('url')

        parse_dict = {
            'username': username,
            'project_url': project_url,
            'project_name': project_name,
            'note': note,
            'event_type': event_type,
            'title': merge_title,
            'url': merge_url
        }

        return parse_dict
