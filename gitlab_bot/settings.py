import telebot
from decouple import config

TOKEN = config('TOKEN')
REDIS_DSN = config('REDIS_DSN', default='redis:6379/0')
PROJECT_URL = config('PROJECT_URL')
bot = telebot.TeleBot(TOKEN)
