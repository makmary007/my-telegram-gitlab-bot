import re
import uuid

import redis

from gitlab_bot.settings import REDIS_DSN


def get_redis_instance():
    redis_conn = re.split(':|/', REDIS_DSN)
    redis_inst = redis.Redis(host=redis_conn[0], port=redis_conn[1],
                             db=redis_conn[2], decode_responses=True)
    return redis_inst

def gen_url_suffix():
    url_suffix = uuid.uuid4()
    return str(url_suffix)
