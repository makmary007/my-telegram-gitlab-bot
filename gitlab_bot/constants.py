from gitlab_bot.parsing.parser import (CommentParser, IssueParser, MergeParser,
                                       PushParser)

PARSE_MAPPING = {
    'issue': IssueParser,
    'push': PushParser,
    'merge_request': MergeParser,
    'note': CommentParser
}
