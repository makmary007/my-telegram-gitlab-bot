import json

import pytest

from gitlab_bot.constants import PARSE_MAPPING

from .constants import MESSAGE_MAPPING


class TestParsing:

    @pytest.fixture(params=['gitlab_bot/fixtures/issues.json', 'gitlab_bot/fixtures/push.json',
                            'gitlab_bot/fixtures/mr.json', 'gitlab_bot/fixtures/comments.json'])
    def get_instance(self, request):
        with open(request.param) as file:
            data = json.load(file)
            object_kind = data['object_kind']
            klass = PARSE_MAPPING[object_kind]
            instance = klass(data)
            return instance

    def test_parser(self, get_instance) -> str:

        instance = get_instance
        correct_msg = MESSAGE_MAPPING[instance.__class__]
        msg = instance.make_message()
        assert msg == correct_msg
