from gitlab_bot.parsing.parser import (CommentParser, IssueParser, MergeParser,
                                       PushParser)

issue_msg = 'Created issue [New API: create/update/delete file](http://example.com/diaspora/issues/23) by Administrator in [Gitlab Test](http://example.com/gitlabhq/gitlab-test) assigned to user1'
push_msg = 'John Smith pushed to [Diaspora](http://example.com/mike/diaspora).\n\nCommits are:\n\n\\* Jordi Mallach: [Update Catalan translation to e38cb41.](http://example.com/mike/diaspora/commit/b6568db1bc1dcd7f8b4d5a946b0b91f9dacd7327)\n\\* GitLab dev user: [fixed readme](http://example.com/mike/diaspora/commit/da1560886d4f094c3e6c9ef40349f7d38b5d27d7)'
mr_msg = 'Administrator opened [MS-Viewport](http://example.com/diaspora/merge_requests/1) in [Gitlab Test](http://example.com/gitlabhq/gitlab-test), assigned to user1.'
comment_msg = 'In MergeRequest [Tempora et eos debitis quae laborum et.](http://example.com/gitlab-org/gitlab-test/merge_requests/1#note_1244) Administrator added comment "This MR needs work." in [Gitlab Test](http://example.com/gitlab-org/gitlab-test)'

MESSAGE_MAPPING = {
    IssueParser: issue_msg,
    PushParser: push_msg,
    MergeParser: mr_msg,
    CommentParser: comment_msg
}
