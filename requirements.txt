astroid==2.4.2
attrs==20.2.0
backcall==0.2.0
certifi==2020.6.20
chardet==3.0.4
click==7.1.2
decorator==4.4.2
Flask==1.1.2
idna==2.10
iniconfig==1.0.1
ipdb==0.13.3
ipython==7.17.0
ipython-genutils==0.2.0
isort==4.3.21
itsdangerous==1.1.0
jedi==0.17.2
Jinja2==2.11.2
lazy-object-proxy==1.4.3
MarkupSafe==1.1.1
mccabe==0.6.1
packaging==20.4
parso==0.7.1
pexpect==4.8.0
pickleshare==0.7.5
pluggy==0.13.1
prompt-toolkit==3.0.6
ptyprocess==0.6.0
py==1.9.0
Pygments==2.6.1
pylint==2.5.3
pyparsing==2.4.7
pyTelegramBotAPI==3.7.2
pytest==6.1.1
python-decouple==3.3
python-dotenv==0.14.0
redis==3.5.3
requests==2.24.0
six==1.15.0
toml==0.10.1
traitlets==4.3.3
urllib3==1.25.10
wcwidth==0.2.5
Werkzeug==1.0.1
wrapt==1.12.1
